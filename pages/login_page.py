from playwright.async_api import Page, async_playwright
import os
from pages.base_page import BasePage


class LoginPage(BasePage):
    def __init__(self, page: Page):
        super().__init__(page)
        self.usernameField = page.locator("#username")
        self.pwField = page.locator("#password")
        self.loginBtn = page.locator("#kc-login")

    async def fill_username(self):
        await self.usernameField.fill(os.environ.get("testuser"))

    async def fill_password(self):
        await self.pwField.fill(os.environ.get("testpwd"))

    async def click_button(self):
        await self.loginBtn.click()
