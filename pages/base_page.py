from playwright.async_api import Page,expect
import os


class BasePage:
    def __init__(self, page: Page):
        self.page = page
        self.uname = os.environ.get("username")
        self.pwd = os.environ.get("password")
        self.acceptCookie = page.locator("xpath=//div[contains(@class,'Footer_cookie-settings-container__ztPMW')]//button")
        self.accountBtn = page.locator("xpath=//div[@class='Header_account-icon-wrapper__hUxIt']")
        self.verifyLoggedOut = page.locator("xpath=//nav[contains(@class,'WalletHeader_navbar')]")

    async def navigate(self):
        await self.page.goto(f"https://{self.uname}:{self.pwd}@cloud-wallet.xfsc.dev/")

    async def accept_cookie(self):
        await self.acceptCookie.click()

    async def click_login(self):
        await self.accountBtn.click()

    async def check_if_logged_out(self):
        await expect(self.verifyLoggedOut).to_have_count(0)

