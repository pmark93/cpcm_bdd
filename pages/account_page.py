from playwright.async_api import Page,expect

from pages.base_page import BasePage


class AccountPage(BasePage):
    def __init__(self, page: Page):
        super().__init__(page)
        self.verifyLoggedIn = page.locator("xpath=//nav[contains(@class,'WalletHeader_navbar')]")

    async def check_if_logged_in(self):
        await expect(self.verifyLoggedIn).to_have_count(1)


