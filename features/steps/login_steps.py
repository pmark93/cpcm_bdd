from behave import given, when, then
from behave.api.async_step import async_run_until_complete

from pages.account_page import AccountPage
from pages.base_page import BasePage
from pages.login_page import LoginPage


@given("the user is on Login page")
@async_run_until_complete
async def open_url(context):
    home_page = BasePage(context.page)
    await home_page.navigate()
    await home_page.accept_cookie()
    await home_page.click_login()


@when("they login with valid credentials")
@async_run_until_complete
async def do_login_steps(context):
    login_page = LoginPage(context.page)
    await login_page.fill_username()
    await login_page.fill_password()
    await login_page.click_button()


@then('the next page is Account page')
@async_run_until_complete
async def is_next_page(context):
    """
    :type context: behave.runner.Context
    """
    account_page = AccountPage(context.page)
    await account_page.check_if_logged_in()


@when('I logout')
@async_run_until_complete
async def click_login_page_button(context):
    """
    :type context: behave.runner.Context
    """
    base_page = BasePage(context.page)
    await base_page.click_login()


@then("I am logged out")
async def verify_logged_out(context):
    """
    :type context: behave.runner.Context
    """
    base_page = BasePage(context.page)
    await base_page.check_if_logged_out()



