Feature: As a user, i want to be able to log in and access the account page

  Scenario: As a user, I can login with valid credentials then logout
    Given the user is on Login page
    When they login with valid credentials
    Then the next page is Account page
    When I logout
    Then I am logged out
