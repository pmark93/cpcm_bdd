Framework taken from : https://github.com/cmoir/playwright_pytest_bdd_example 
Repo is : https://gitlab.eclipse.org/pmark93/cpcm_bdd.git

Prerequisites:
<ul>
<li>Python installed</li>
<li>pip installed</li>
</ul>

Steps to run :
<ol>
<li>Cd into your desired folder and clone the repo</li>
<li>Create a venv : python -m venv %venv_name%</li>
<li>In terminal or IDE cd into your folder and activate venv</li>
    <ul>
        <li>WINDOWS : %venv_name%\Scripts\activate</li>
        <li>MAC : source %venv_name%/bin/activate</li>
    </ul>
<li>Install dependencies:</li>
    <ul>
        <li>pip install playwright behave</li>
        <li>playwright install-deps</li>
    </ul>
<li>cd into bdd folder</li>
<li>run behave -n 'As a user, I can login with valid credentials then logout'</li>
</ol>